import './App.css';
import Navbar from './components/Navbar';
import CarouselComp from './components/inc/CarouselComp';
//import MapComp from './components/inc/MapComp';
import CardEmail from './components/inc/CardEmail';
import CardPhone from './components/inc/CardPhone';
import CardMap from './components/inc/CardMap';
import ButtomInsta from './components/inc/ButtomInsta';
import {Container, Row, Col } from 'react-bootstrap';
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import React from'react';
import Inicio from './pages/home';
import Nosotros from './pages/nosotros';
import { BrowserRouter, Routes } from 'react-router-dom';
import {
 Switch,
 Route,
 Link
} from "react-router-dom";
import { render } from '@testing-library/react';


function App() {
  return (
    <BrowserRouter>
      <Navbar/>
      <Routes>
        <Route path='/home' element={<Inicio/>}/>
        <Route path='/nosotros' element={<Nosotros/>}/>
        <Route path='/servicios' />
        <Route path='/contacto' />
      </Routes>
    </BrowserRouter>
  );
}

export default App;