import React from "react";
import "../components/css/Maps.css";
import GoogleMapReact from "google-map-react";
import MyMarker from "../components/inc/MyMarker.jsx"

const points = [
    { id: 1, title: "Round Pond", lat: -33.41952136760165, lng: -70.60075044233842 } , 
  ];

const Nosotros = () => {
  return (
<div className="App">
      <GoogleMapReact
        bootstrapURLKeys={{
          // remove the key if you want to fork
          key: "AIzaSyCxiqQJ_4SPbYx_qtkO6CvK8e0sm7WMgzY",
          language: "en",
          region: "US"
        }}
        defaultCenter={{ lat: -33.41952136760165, lng: -70.60075044233842 }}
        defaultZoom={15}
      >
        {points.map(({ lat, lng, id, title }) => {
          return (
            <MyMarker key={id} lat={lat} lng={lng} text={id} tooltip={title} />
          );
        })}
      </GoogleMapReact>
    </div>
  )
};

export default Nosotros;